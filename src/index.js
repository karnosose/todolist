class ToDo {
  constructor() {
    this.app = document.querySelector("#app");
    this.input = document.querySelector("#addTask");
    this.header = document.querySelector("header");
    this.tasksList = document.querySelector("#tasksList");
    this.footer = document.querySelector("footer");

    this.toDoList = localStorage.getItem("toDoList")
      ? JSON.parse(localStorage.getItem("toDoList"))
      : [];

    this.printToDoList();

    this.addToDoEventListerner();

    this.refreshFilters();

    this.filters();
  }

  printToDoList() {
    this.tasksList.textContent = "";
    this.toDoList.map(item => this.createListItem(item.completed, item.value));
  }

  addToDo() {
    // add to lacal storage
    const todoItem = {
      value: this.input.value,
      completed: false
    };

    if (this.input.value.length > 0) {
      this.toDoList.push(todoItem);
      localStorage.setItem("toDoList", JSON.stringify(this.toDoList));

      // create html element
      this.createListItem(this.toDoList.length, this.input.value);
    }
    this.input.value = "";

    this.refreshActiveItemsCount();
  }

  removeToDo(div) {
    const self = this;
    let removeKey = null;

    //remove from to do item from DOM
    Object.keys(self.toDoList).forEach(key => {
      if (div.childNodes[1].textContent === self.toDoList[key].value) {
        div.parentNode.remove();
        removeKey = key;
      }
    });

    //remove to do item from storage
    if (removeKey !== null) {
      self.toDoList.splice(removeKey, 1);
      localStorage.setItem("toDoList", JSON.stringify(self.toDoList));
    }

    this.refreshActiveItemsCount();
  }

  createListItem(checked, text) {
    const self = this;
    const task = document.createElement("li");
    const div = document.createElement("div");
    const checkInput = document.createElement("input");
    checkInput.setAttribute("type", "checkbox");
    const label = document.createElement("label");
    const button = document.createElement("button");
    button.textContent = "X";
    button.setAttribute("class", "remove");
    task.setAttribute("class", "task");
    div.appendChild(checkInput);
    div.appendChild(label);
    div.appendChild(button);
    label.textContent = text;
    task.appendChild(div);
    this.tasksList.appendChild(task);

    if (checked == "true") {
      checkInput.checked = true;
      task.classList.add("completed");
    }

    // add event listener for this item
    button.addEventListener("click", e => {
      self.removeToDo(e.target.parentNode);
      self.refreshFilters();
    });

    checkInput.addEventListener("click", e => {
      self.markCheckedToDo(e);
    });
    this.refreshActiveItemsCount();
  }

  addToDoEventListerner() {
    const self = this;

    this.input.addEventListener("keypress", function(e) {
      const key = e.which || e.keyCode;
      if (key === 13) {
        self.addToDo();
        self.refreshFilters();
      }
    });
  }

  filters() {
    const tasks = document.getElementsByClassName("task");
    const self = this;
    // filter completed toDos
    const completed = document.querySelector(".filter-completed");
    completed.addEventListener("click", () => {
      for (let i = 0; i < tasks.length; i++) {
        if (tasks[i].classList.contains("completed")) {
          tasks[i].style.display = "block";
        } else {
          tasks[i].style.display = "none";
        }
      }
    });

    // filter all toDos
    const filterAll = document.querySelector(".filter-all");
    filterAll.addEventListener("click", () => {
      for (let i = 0; i < tasks.length; i++) {
        tasks[i].style.display = "block";
      }
    });

    // filter active toDos
    const active = document.querySelector(".filter-active");
    active.addEventListener("click", () => {
      for (let i = 0; i < tasks.length; i++) {
        if (tasks[i].classList.contains("completed")) {
          tasks[i].style.display = "none";
        } else {
          tasks[i].style.display = "block";
        }
      }
    });

    //set active status to filters
    const filters = document.querySelectorAll(".filter");

    for (let i = 0; i < filters.length; i++) {
      filters[i].addEventListener("click", () => {
        for (let i = 0; i < filters.length; i++) {
          if (filters[i].classList.contains("active")) {
            filters[i].classList.remove("active");
          }
        }
        filters[i].classList.add("active");
      });
    }

    // clear completed toDos
    const clear = document.querySelector(".clear");
    clear.addEventListener("click", () => {
      self.clearCompleted();
    });
  }

  clearCompleted() {
    this.toDoList = this.toDoList.filter(item => item.completed === false);
    localStorage.setItem("toDoList", JSON.stringify(this.toDoList));
    this.printToDoList();
    this.refreshActiveItemsCount();
  }

  refreshFilters() {
    if (this.toDoList.length == 0) {
      this.footer.style.display = "none";
    } else {
      this.header.setAttribute("class", "toggleAll");
      this.footer.style.display = "block";
    }
  }

  refreshActiveItemsCount() {
    const activeToDos = this.toDoList.filter(item => item.completed === false);
    const itemsLeft = document.querySelector(".activeItems");
    itemsLeft.textContent = activeToDos.length;
  }

  markCheckedToDo(e) {
    const div = e.target.parentNode;
    const self = this;
    if (e.target.checked) {
      div.parentNode.classList.add("completed");
      Object.keys(self.toDoList).forEach(key => {
        if (div.childNodes[1].textContent === self.toDoList[key].value) {
          self.toDoList[key].completed = "true";
          localStorage.setItem("toDoList", JSON.stringify(self.toDoList));
        }
      });
    } else {
      div.parentNode.classList.remove("completed");
      Object.keys(self.toDoList).forEach(key => {
        if (div.childNodes[1].textContent === self.toDoList[key].value) {
          self.toDoList[key].completed = "false";
          localStorage.setItem("toDoList", JSON.stringify(self.toDoList));
        }
      });
    }
    this.refreshActiveItemsCount();
  }
}

const todo = new ToDo();
